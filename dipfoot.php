	<!-- begin::Footer -->
			


         <footer class="m-grid__item		m-footer " style="margin-left: 0px">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								2019 &copy; AdsHub
							</span>
						</div>
						<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
							<ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
								<li class="m-nav__item">
									<a href="#" class="m-nav__link">
										<span class="m-nav__link-text">About</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link">
										<span class="m-nav__link-text">Privacy</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link">
										<span class="m-nav__link-text">T&C</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link">
										<span class="m-nav__link-text">Purchase</span>
									</a>
								</li>
								<li class="m-nav__item m-nav__item">
									<a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">
										<i class="fa fa-question-circle-o" style="font-size:24px; position:relative;right: 5px;top: 2px;color: #c1bfd0;" aria-hidden="true"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</footer>
			<!-- end::Footer -->
		</div>

		<!-- end:: Page -->

		

		<!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="fa fa-arrow-up" aria-hidden="true"></i>
		</div>

		<!-- end::Scroll Top -->

		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet"/>

	<!--begin:: Global Mandatory Vendors -->
		<script src="assets/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="assets/vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="assets/vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="assets/vendors/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="assets/vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="assets/vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="assets/vendors/wnumb/wNumb.js" type="text/javascript"></script>

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<script src="assets/vendors/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="assets/vendors/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="assets/vendors/jquery.repeater/src/repeater.js" type="text/javascript"></script>
		<script src="assets/vendors/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="assets/vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js" type="text/javascript"></script>
		<script src="assets/vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
		<script src="assets/vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
		<script src="assets/vendors/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
		<script src="assets/vendors/handlebars/dist/handlebars.js" type="text/javascript"></script>
		<script src="assets/vendors/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
		<script src="assets/vendors/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="assets/vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
		<script src="assets/vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js" type="text/javascript"></script>
		<script src="assets/vendors/nouislider/distribute/nouislider.js" type="text/javascript"></script>
		<script src="assets/vendors/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
		<script src="assets/vendors/autosize/dist/autosize.js" type="text/javascript"></script>
		<script src="assets/vendors/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
		<script src="assets/vendors/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
		<script src="assets/vendors/dropzone/dist/dropzone.js" type="text/javascript"></script>
		<script src="assets/vendors/summernote/dist/summernote.js" type="text/javascript"></script>
		<script src="assets/vendors/markdown/lib/markdown.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js" type="text/javascript"></script>
		<script src="assets/vendors/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
		<script src="assets/vendors/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script>
		<script src="assets/vendors/toastr/build/toastr.min.js" type="text/javascript"></script>
		<script src="assets/vendors/jstree/dist/jstree.js" type="text/javascript"></script>
		<script src="assets/vendors/raphael/raphael.js" type="text/javascript"></script>
		<script src="assets/vendors/morris.js/morris.js" type="text/javascript"></script>
		<script src="assets/vendors/chartist/dist/chartist.js" type="text/javascript"></script>
		<script src="assets/vendors/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
		<script src="assets/vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
		<script src="assets/vendors/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
		<script src="assets/vendors/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
		<script src="assets/vendors/counterup/jquery.counterup.js" type="text/javascript"></script>
		<script src="assets/vendors/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
		<script src="assets/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Bundle -->
		<script src="assets/demo/base/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors -->
		<script src="assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts -->
		<script src="assets/app/js/dashboard.js" type="text/javascript"></script>


<script src="assets/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
		<script src="assets/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
		<script src="assets/custom/crud/wizard/wizard.js" type="text/javascript"></script>
		
	

	

		<script src="assets/app/js/nicEdit.js" type="text/javascript"></script>

		<script src="assets/custom/crud/forms/widgets/autosize.js" type="text/javascript"></script>


		<!--end::Page Scripts -->
		
			
		<script>
		
		
// Get the modal
var publicationModal = document.getElementById('myModal');
var costBreakUpModal = document.getElementById('modalTwo');
var resetUpModal = document.getElementById('resetModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");
var costBrkbtn = document.getElementById("costBreakUpBtn");
var resetBtn = document.getElementById("resetBtn");

// Get the <span> element that closes the modal

// When the user clicks the button, open the modal 

if(btn!=null){
	btn.onclick = function() {
		publicationModal.style.display = "block";
	}
}

if(costBrkbtn!=null){
	costBrkbtn.onclick = function() {
		costBreakUpModal.style.display = "block";
	}
}


if(resetBtn!=null){
	resetBtn.onclick = function() {
		resetUpModal.style.display = "block";
	}
}


var pSpan = document.getElementsByClassName("pClose")[0];
pSpan.onclick = function() {
	publicationModal.style.display = "none";
}

var rSpan = document.getElementsByClassName("rClose")[0];
rSpan.onclick = function() {
	resetUpModal.style.display = "none";
}


var cbSpan = document.getElementsByClassName("cBClose")[0];
cbSpan.onclick = function() {
	costBreakUpModal.style.display = "none";
}



		var $publication = $('#m_select_publication');
		var $cities = $('#m_select_city');

		 
		
		$(document).ready(function() {
			
			  
			 $publication.select2({
		            placeholder: "Select Publication",
		        }).val([1]).trigger("change");
			 
			 $publication.change(function(event) {
				 var items= $(this).val();
		    	updateCityList(items.toString());
		    	  
		      });
			 
			 
			 	
				
				var headerStyle="background: null;color: null";
				$('#editHeaderDivId').children('div').eq(1).attr("style", headerStyle );
				 $('#editHeaderDivId').children('div').eq(1).children('div').attr("id", "headerEditor");
				 $('#previewHeaderT1').attr("style", headerStyle );
				 
				var bodyStyle="background: null;color: null";
				 $('#editBodyDivId').children('div').eq(1).attr("style", bodyStyle ); 
				 $('#editBodyDivId').children('div').eq(1).children('div').attr("id", "bodyEditor");
				 $('#previewBodyT1').attr("style", bodyStyle );
				 
				var footerStyle="background: null;color: null";
				 $('#editFooterDivId').children('div').eq(1).attr("style", footerStyle );
				 $('#editFooterDivId').children('div').eq(1).children('div').attr("id", "footerEditor");
				 $('#previewFooterT1').attr("style", footerStyle );
				 
				 var mainDivStyle="border: 0px solid #000 !important;width: 0cm; font-family:Arial, Helvetica, sans-serif;cursor: pointer;line-height:20px";
				 $('#previewMainId').attr("style", mainDivStyle );
				  
				 $('#m_select2_3').select2({
			            placeholder: "Select category",
			        }).val([1]).trigger("change"); 
				 
				 //$('#m_select2_3').val('1');
				 $('#borderPixel').val('1');
				   
				
					 $('.previewHeaderImage').hide();
					 $('#headerImageUploadId').hide();
					 
					 $("#header_image_id").attr("src",'');
					 $('#headerImage').val('');
					 
				 
				
				
					$('.previewBodyImage').hide();
					 $('#bodyImageUploadId').hide();
					 
					 $("#body_image_id").attr("src",'');
					 $('#bodyImage').val('');
					 
				
				
				$('#selAdTemplateId').val('0');
				
			
			
			 
    });
		
	
	
		 $('#m_select_publication').change(function(event) {

	    	  var pubId= $(this).val();

	    	  console.log(pubId);
	    	  
	    	  if(pubId!=null){
	    		  var jsonEditionObject = []; 
	        	   
	       		  $.ajax({  
	    		       url : '/ads/geteditionlist',  
	    		       type : 'POST',
	    		       dataType: 'json',
	    		       data: { adCategoryId: '1', publicationId:pubId},
	    		       success : function(response) {  
	    		    	   jQuery(response).each(function(i, item){
	    		    		   console.log(item);

	    		    		   edition = {}
	    		    		    
	    		    		  // edition ["id"] = item.editionId;
	    		    		   edition ["id"] = item.city.cityId;
	    		    		   edition ["text"] = item.city.name;
	    		    		   jsonEditionObject.push(edition);
	    		    		});
	    		    	   
	      
	    		     		 
	    		     		if(jsonEditionObject.length>0){
	    		     				$cities.select2({
	    	                		  placeholder: "Select City",
	    	                		  data: jsonEditionObject
	    	                		});
	    		     		}
	    		     		
	    		     		
	    		     		  
	    		       } 
	    			}); 
	    	  }else{
	    		  $cities.html('').select2({
		     			allowClear: true,
		     			 placeholder: "Select City",
		     			data: {id:null, text: null}
		     		});
	    	  }
	    	  
	      });
		 
		 updateCityList = function(pubId){


	    	  console.log(pubId);
	    	  
	    	  if(pubId!=null){
	    		  var jsonEditionObject = []; 
	        	   
	       		  $.ajax({  
	    		       url : '/ads/geteditionlist',  
	    		       type : 'POST',
	    		       dataType: 'json',
	    		       data: { adCategoryId: '1', publicationId:pubId},
	    		       success : function(response) {  
	    		    	   jQuery(response).each(function(i, item){
	    		    		   console.log(item);

	    		    		   edition = {}
	    		    		    
	    		    		  // edition ["id"] = item.editionId;
	    		    		   edition ["id"] = item.city.cityId;
	    		    		   edition ["text"] = item.city.name;
	    		    		   jsonEditionObject.push(edition);
	    		    		});
	    		    	   
	      
	    		     		 
	    		     		if(jsonEditionObject.length>0){
	    		     				$cities.select2({
	    	                		  placeholder: "Select City",
	    	                		  data: jsonEditionObject
	    	                		});
	    		     		}
	    		     		
	    		     		
	    		     		  
	    		       } 
	    			}); 
	    	  }else{
	    		  $cities.html('').select2({
		     			allowClear: true,
		     			 placeholder: "Select City",
		     			data: {id:null, text: null}
		     		});
	    	  }
	    	  
	      };
		 
	      
	      
	      
		      $('#sel_date').datepicker({
					multidate: true,
					format: 'dd M',
					 orientation: "bottom right",
					startDate: '+1d',
					beforeShowDay: function(date) {
						if(date.getDay() == 0){
							return false;  
						}else{
							return true;
						} 
					}
					
		
				}).on("changeDate", function (e) {
					var selValues = $(this).val();
					console.log(selValues);
				   $("#selDate").text(selValues);
				   $('#sel_hdate').val(selValues);
				   $('#sel_date').val('');
				}); 

	      
	      
	      designAd = function(){
				var isFormValid = true;

				if($('#selDate').text().trim().length==0){
						  $('#selDateTxt').addClass('m--font-danger');
						isFormValid = false;
					}
				
				if($('#m_select_publication').val()===null){
					$('#help_text_publication').addClass('m--font-danger');
					isFormValid = false;
				}
				
				if($('#m_select_city').val()===null){
					$('#help_text_publication').addClass('m--font-danger');
					isFormValid = false;
				}
				 
				if(isFormValid){
					$('#ads-publish-form').submit();
				}
			}
	       	
	      
		
		updatePublicationList = function(cityIds){
			 var jsonPublicationObject = [];
			 
			$.ajax({  
  		       url : '/ads/getpublicationlist',  
  		       type : 'POST',
  		       dataType: 'json',
  		       data: { aCityList: cityIds},
  		       success : function(response) {  
  		    	   jQuery(response).each(function(i, item){
  		    		   
  		    		    publication = {}
  		    		    
  		    		    publication ["id"] = item.publicationId;
  		    		    publication ["text"] = item.name;
  		    	        jsonPublicationObject.push(publication);
  		    		});
  		    	   
 			     		$publication.html('').select2({
 			     			allowClear: true,
 			     			 placeholder: "Select Publication",
 			     			data: {id:null, text: null}
 			     		});
    
  		     		 
  		     		if(jsonPublicationObject.length>0){
  		     				$publication.select2({
  	                		  placeholder: "Select Publication",
  	                		  data: jsonPublicationObject
  	                		});
  		     		}
  		     		  
  		       } 
  			});
		}
		
		
		searchForm = function(){
			var isFormValid = true;
			
			if($('#m_select_city').val().length==0){
				$('#help_text_city').addClass('m--font-danger');
				isFormValid = false;
			}
			 
			if($('#m_select_publication').val()===null){
				$('#help_text_publication').addClass('m--font-danger');
				isFormValid = false;
			}
			
			if($('#m_select_type').val()===null){
				$('#help_text_type').addClass('m--font-danger');
				isFormValid = false;
			}
			
			if($('#m_select_category').val()===null){
				$('#help_text_category').addClass('m--font-danger');
				isFormValid = false;
			}
			
			if(isFormValid){
				$('#ads-search-form').submit();
			}
		}
		
		
		$('.selAdId1').click(function() {
			var mainDivStyle = $(this).attr('style');
			var adTemplateId =  $(this).attr('data-id');
			console.log('adTemplateId : '+adTemplateId);
			$('#selAdTemplateId').val(adTemplateId);
			
			$('#previewMainId').attr("style", mainDivStyle );
			
			 var headerText = $(this).find( ".headerT1" ).html();
			 var headerStyle = $(this).find( ".headerT1" ).attr('style');
			 
			 var bodyText = $(this).find( ".bodyT1" ).html().trim();
			 var bodyStyle = $(this).find( ".bodyT1" ).attr('style');
			 
			 var footerText = $(this).find( ".footerT1" ).html().trim();
			 var footerStyle = $(this).find( ".footerT1" ).attr('style');
			 
			 var headerImage = $(this).find( ".headerImage" ).find('img').attr('src');
			 var bodyImage = $(this).find( ".bodyImage" ).find('img').attr('src');
			 
			 nicEditors.findEditor( "headerTextArea" ).setContent(headerText);
			nicEditors.findEditor( "bodyTextArea" ).setContent(bodyText);
			 nicEditors.findEditor( "footerTextArea" ).setContent(footerText);
			 
			 $("#previewHeaderT1").html(headerText);
			 $('#previewHeaderT1').attr("style", headerStyle );
			 $('#editHeaderDivId').children('div').eq(1).attr("style", headerStyle );
			 $('#editHeaderDivId').children('div').eq(1).children('div').attr("id", "headerEditor");
			 
			 $("#previewBodyT1").html(bodyText); 
			 $('#previewBodyT1').attr("style", bodyStyle );
			 $('#editBodyDivId').children('div').eq(1).attr("style", bodyStyle );
			 $('#editBodyDivId').children('div').eq(1).children('div').attr("id", "bodyEditor");
			 
			 $("#previewFooterT1").html(footerText);
			 $('#previewFooterT1').attr("style", footerStyle );
			 $('#editFooterDivId').children('div').eq(1).attr("style", footerStyle );
			 $('#editFooterDivId').children('div').eq(1).children('div').attr("id", "footerEditor");
			  
			 if (headerImage === undefined){
				 $('.previewHeaderImage').hide();
				 $('#headerImageUploadId').hide();
				 
				 $("#header_image_id").attr("src",'');
				 $('#headerImage').val('');
				 
			 }else{
				 $('.previewHeaderImage').show();
				 $('#headerImageUploadId').show();
				 $("#header_image_id").attr("src",headerImage);
			 }
			 
			 if (bodyImage === undefined){
				 $('.previewBodyImage').hide();
				 $('#bodyImageUploadId').hide();
				 
				 $("#body_image_id").attr("src",'');
				 $('#bodyImage').val('');
				 
			 }else{
				 $('.previewBodyImage').show();
				 $('#bodyImageUploadId').show();
				 $("#body_image_id").attr("src",bodyImage);
			 }
			 
		    $('#templateId').hide();
			$('#editTemplateId').show();
			$('#pricingDetailId').show();
			
			 
			 $('#m_select2_3').val('1');
			 $('#borderPixel').val('1');
			 
			calculatePrice();
		}); 

		
		$('.showTemplate').click(function() {
		    $('#templateId').show();
			$('#editTemplateId').hide();
			$('#pricingDetailId').hide();
		});
		
		bkLib.onDomLoaded(function(){
			  var headerEditor = new nicEditor().panelInstance('headerTextArea');
			  headerEditor.addEvent('blur', function() {
				  var enteredText = $(this).html();
				  $("#previewHeaderT1").html(enteredText);
				  calculatePrice();
			});
			  
			  headerEditor.addEvent('focus', function() {
				  var enteredText = $(this).html();
				  $("#previewHeaderT1").html(enteredText);
				  calculatePrice();
			});

		});
			 bkLib.onDomLoaded(function(){	  
			  var bodyEditor = new nicEditor().panelInstance('bodyTextArea');
			  bodyEditor.addEvent('blur', function() {
				  var enteredText = $(this).html();
				  $("#previewBodyT1").html(enteredText);
				  calculatePrice();
			});
			  
			  bodyEditor.addEvent('focus', function() {
				  var enteredText = $(this).html();
				  $("#previewBodyT1").html(enteredText);
				  calculatePrice();
			});
			  
			  });
			  bkLib.onDomLoaded(function(){  
			  var footerEditor = new nicEditor().panelInstance('footerTextArea');
			  
			  footerEditor.addEvent('blur', function() {
				  var enteredText = $(this).html();
				  $("#previewFooterT1").html(enteredText);
				  calculatePrice();
			});
			  
			  footerEditor.addEvent('focus', function() {
				  var enteredText = $(this).html();
				  $("#previewFooterT1").html(enteredText);
				  calculatePrice();
			});
			  
		});
		
		 
		var nicEditorConfig = bkClass.extend({
			buttons : {
				'bold' : {name : __('Click to Bold'), command : 'Bold', tags : ['B','STRONG'], css : {'font-weight' : 'bold'}, key : 'b'},
				'italic' : {name : __('Click to Italic'), command : 'Italic', tags : ['EM','I'], css : {'font-style' : 'italic'}, key : 'i'},
				'underline' : {name : __('Click to Underline'), command : 'Underline', tags : ['U'], css : {'text-decoration' : 'underline'}, key : 'u'},
				'left' : {name : __('Left Align'), command : 'justifyleft', noActive : true},
				'center' : {name : __('Center Align'), command : 'justifycenter', noActive : true},
				'right' : {name : __('Right Align'), command : 'justifyright', noActive : true},
				'justify' : {name : __('Justify Align'), command : 'justifyfull', noActive : true},
				'ol' : {name : __('Insert Ordered List'), command : 'insertorderedlist', tags : ['OL']},
				'ul' : 	{name : __('Insert Unordered List'), command : 'insertunorderedlist', tags : ['UL']},
				'subscript' : {name : __('Click to Subscript'), command : 'subscript', tags : ['SUB']},
				'superscript' : {name : __('Click to Superscript'), command : 'superscript', tags : ['SUP']},
				'strikethrough' : {name : __('Click to Strike Through'), command : 'strikeThrough', css : {'text-decoration' : 'line-through'}},
				'removeformat' : {name : __('Remove Formatting'), command : 'removeformat', noActive : true},
				'indent' : {name : __('Indent Text'), command : 'indent', noActive : true},
				'outdent' : {name : __('Remove Indent'), command : 'outdent', noActive : true},
				'hr' : {name : __('Horizontal Rule'), command : 'insertHorizontalRule', noActive : true}
			},
			iconsPath : 'https://s3.ap-south-1.amazonaws.com/adshub-adwit/HTML_ASSETS/assets_new/images/nicEditorIcons.gif',
			buttonList : ['save','bold','italic','underline','left','center','right','justify','fontSize','fontFamily'],
			iconList : {"bgcolor":1,"forecolor":2,"bold":3,"center":4,"hr":5,"indent":6,"italic":7,"justify":8,"left":9,"ol":10,"outdent":11,"removeformat":12,"right":13,"save":24,"strikethrough":15,"subscript":16,"superscript":17,"ul":18,"underline":19,"image":20,"link":21,"unlink":22,"close":23,"arrow":25},
		});

	 
		var inst = setInterval(change, 500);

		function change() {
			var selDivId = $('.nicEdit-selected').attr('id');
			var enteredText = $('.nicEdit-selected').html();
			console.log('selDivId : '+selDivId);
			
			if(selDivId==='headerEditor'){
				 $("#previewHeaderT1").html(enteredText);
			}else if(selDivId==='bodyEditor'){
				 $("#previewBodyT1").html(enteredText);
			}else if(selDivId==='footerEditor'){
				 $("#previewFooterT1").html(enteredText);
			}

			calculatePrice();
		}
		
		$('#m_select2_3').on('change', function() {
			 
			$('#borderPixel').val(this.value);
			
			$("#previewMainId").css("border-width", this.value+"px");
			  calculatePrice();
		}); 
		
		
		$('#headerImageUploadForm').submit(function(evt) {

            evt.preventDefault();

            var formData = new FormData(this);

            $.ajax({
	            type: 'POST',
	            url: '/ads/updateImage',
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            success: function(data) {
	               console.log('success : '+data);
	               $("#header_image_id").attr("src",'https://s3.ap-south-1.amazonaws.com/adshub-adwit/Ads/'+data);
	               $('#headerImage').val(data);
	               calculatePrice();
	            },
	            error: function(data) {
	            	console.log('error : '+data);
	            	calculatePrice();
	            } 
            });
        });
		
		
		$('#bodyImageUploadForm').submit(function(evt) {

            evt.preventDefault();

            var formData = new FormData(this);

            $.ajax({
	            type: 'POST',
	            url: '/ads/updateImage',
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            success: function(data) {
	               console.log('success : '+data);
	               $("#body_image_id").attr("src",'https://s3.ap-south-1.amazonaws.com/adshub-adwit/Ads/'+data);
	               $('#bodyImage').val(data);
	               calculatePrice();
	            },
	            error: function(data) {
	            	console.log('error : '+data);
	            	calculatePrice();
	            } 
            });
        });
		
		
		calculatePrice = function(){
			var price = 735.0;
			var width = 3;
			var minHeight = 5; 
			
			console.log('price : '+price);
			console.log('width : '+width);
			console.log('minHeight : '+minHeight);
			
			var height = Math.floor($('#previewMainId').height()*0.02645833);
			//console.log('height : '+(height));
			
			if(height>minHeight){
				$('#adHeighInCmId').html(height);
			}else{
				height = minHeight
				$('#adHeighInCmId').html(minHeight);
			}
			
			
			
			var adCostPrice = parseInt(price)*height*width;
			$('#adCostPrice').html(adCostPrice.toLocaleString());
			
			var serviceCost = Math.round(parseInt(adCostPrice)*.05);
			$('#price_ServiceCharge').html(serviceCost.toLocaleString()); 
			 
			var totalPrice = parseInt(adCostPrice)+parseInt(serviceCost);
			
			$('#totalCostPrice').html(totalPrice.toLocaleString());
			$('#totalAdCostId').html(totalPrice.toLocaleString());
			
			
		}
		
		
		publishDesign = function(){
			
			var finalPrice = $('#totalAdCostId').text();
			console.log('finalPrice : '+finalPrice);
			
			$('#totalHPrice').val(finalPrice);
			
			
			var height = Math.floor($('#previewMainId').height()*0.02645833);
			console.log('height : '+(height));
			
			$('#adWidth').val('3');
			$('#adHeight').val(height);
			 
			$('#headerText').val($('#headerEditor').html());
			$('#bodyText').val($('#bodyEditor').html());
			$('#footerText').val($('#footerEditor').html());
			
			$('#ad-design-from').submit();
		}
		
		
		
		removeAdEdition = function(userAdEditionId){
			 
			$.ajax({  
	  		       url : '/displayadcateogory/removeedition',  
	  		       type : 'POST',
	  		       data: { userAdEditionId: userAdEditionId},
	  		    	 success: function(response) {   
	  		    		window.location.href = "/displayad/adcateogory/matrimonial/RxxOBLR7mPM=/";
					}
	  			});
			
		}
		
		
removeAdPublication = function(){ 
			
			$.ajax({  
	  		       url : '/adcateogory/removepublication',  
	  		       type : 'POST',
	  		    	 success: function(response) {   
	  		    		window.location.href = "/displayad/adcateogory/matrimonial/RxxOBLR7mPM=/";
					}
	  			});
			
		}
		
		 
		</script>
	</body>

	<!-- end::Body -->
</html>