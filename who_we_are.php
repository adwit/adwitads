<?PHP
 include 'header.php';
?>

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" style="padding-left: 0px">

				
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
				<h2 class=" m--margin-top-30 " style="text-align:center;">Who we are</h2>
				<div class="row side_padng">
				
				  <div class="col-md-6">
					<div class="m-content cont_padng">
                      <div class="m-portlet stat_page">
						<div class="m-portlet__body news_height">
						<h6 class="txt_news red m--margin-top-10">About Newspaperads.in</h6>
						<h6 class="txt_news">Newspaperads.in is a newspaper ad booking platform developed by the technology wing of Adwit. Newspaperads.in is a one-stop portal that can be used to design and book newspaper ads in more than 120 newspapers across the country. The technology platform offers a self-service design tool, with an automated pricing algorithm to give advertisers real-time views of the design and rates.
</h6>
						<h6 class="txt_news">Newspaperads.in offers both advertisers and agencies the ease of designing and booking ads, without having to call, email or visit a newspaper office. 
</h6>
						<h6 class="txt_news ">At this time, Newspaperads.in is focused on the classified text and classified display ads for English newspapers. A tool to design and book display ads for retail businesses is being developed and will be launched shortly.
</h6>
							</div>
							
                           </div>
					</div>
					</div>
					 <div class="col-md-6">
					<div class="m-content cont_padng">
                      <div class="m-portlet stat_page">
						<div class="m-portlet__body news_height">
						<h6 class="txt_news red m--margin-top-10">About Adwit</h6>
						
					<h6 class="txt_news">Adwit India is a full-service advertising agency that began its operations in 1979. Over the past four decades it has grown to 5 locations across the country. Having successfully created and deployed marketing campaigns for several hundred clients, our multiple business verticals cater to all marketing needs. 
</h6>
						<h6 class="txt_news ">
					In addition to an India focused division, Adwit Global creates graphic designs for several hundred newspapers across North America. Adwit Digital is the digital wing that helps businesses create websites, software systems and digital media campaigns.
							</h6></div>
							</div>
                           </div>
					</div>
				 </div>
				</div>
			</div>

			<!-- end:: Body -->

<?PHP
 include 'footer.php';
?>