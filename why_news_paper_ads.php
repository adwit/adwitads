<?PHP include 'header.php';
?>

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" style="padding-left: 0px">

				
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
				<h2 class=" m--margin-top-30 " style="text-align:center;">Why advertise in newspapers?</h2>
				<div class="row side_padng">
				
				  <div class="col-md-6 offset-md-3">
					<div class="m-content cont_padng">
                      <div class="m-portlet stat_page">
						<div class="m-portlet__body news_height1 m--margin-top-30">
						
<h6 class="txt_news ">The first statement we hear when people find out we are in the newspaper ad business is ‘Newspapers are dying (or dead). Digital is the way forward.’ </h6>
<h6 class="txt_news ">We beg to differ. Newspapers are the bedrock of society. They are a trusted source, physically present in multiple locations and are responsible for material that is printed. </h6>
<h6 class="txt_news ">With Tier II and Tier III cities expanding and the educated population increasing Y-o-Y, readers are beginning to make selective and conscious decisions regarding their sources of information. Fake news is a term being bandied about because of the misinformation being spread on social and digital media channels. In addition ad blockers, spammers and phishing emails/links are making readers weary of digital media channels.</h6>
<h6 class="txt_news ">In India, newspaper readership and advertising is actually growing. Even large online businesses (yes you, Amazon and Flipkart) regularly advertise in newspapers.</h6>
<h6 class="txt_news ">Newspaper ads provide one of the lowest costs per reach among all available advertising options. </h6>
<h6 class="txt_news ">Localization is another key feature of newspaper advertising. Local businesses can advertise in newspapers and reach out to a spectrum of local population. This helps increase overall response and conversion rates.</h6>
							</div>
							
                           </div>
					</div>
					</div>

				 </div>
				</div>
			</div>

			<!-- end:: Body -->

			<!-- begin::Footer -->
			

<?PHP
 include 'footer.php';
?>