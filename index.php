<?php
include 'database.php';
include 'header.php';
unset($_SESSION['date']);
unset($_SESSION['s_cities']);
unset($_SESSION['s_dates']);
unset($_SESSION['allcity']);
unset($_SESSION['alldate']);
unset($_SESSION['bgColor']);
unset($_SESSION['tick_mark']);
unset($_SESSION['selected_ad_adPreviewId']);
unset($_SESSION['selected_ad_text_compose']);
unset($_SESSION['selected_totalAdCostId']);
?>


			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" style="padding-left: 0px">

				
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<div class="m-subheader-search">
						<div class="row">
						
						<div class="col-xl-8 col-md-8 col-sm-12 col-xs-12 " style="    margin-top: 80px;    padding-left: 0px;">
								<h1 class="m-subheader-search__title" >Book classified & display advertisements</br> across leading newspapers in 3 easy steps</h1>
						
						</div>
						<div class="col-md-4">
							<img src="assets/images/reading-newspaper.png" style="width: 100%;">
						</div>
						</div>
					</div>
					
					<div class="m-content">

						<div class="m-portlet">
						<div class="m-portlet__body">
							
								<div class="row m--margin-bottom-30 m--margin-top-30">
									<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
					               </div>
								<div class="col-xl-8 col-md-12 col-sm-12 col-xs-12">
									<nav>
                                       <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
					                            <h4 class="m-widget14__title ">
													Classified text ad
												</h4>
												<h6 class="m-widget14__title titlesml" >Click below category</h6>
												
											 </a>
                                             <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
												<h4 class="m-widget14__title" >
													Classified display ad
												</h4>
												<h6 class="m-widget14__title titlesml" >Click below category</h6>
												
											  </a>
									      </div>
									  </nav>
									</div>
								</div>
				
				
                  
                  <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
					<div class="row">
					<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
					  </div>
                      	
                      <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="step1.php?tab=classified_text&id=1" style="background: url('assets/images/Matrimonial.jpg') no-repeat scroll left center;
													" aria-controls="where"   id="MATR-MAIN">
												<img src="assets/images/Matrimonial.jpg" style="display:none">
												<span class="imgspan"> Matrimonial </span>
												</a>
											
											</div>
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
											
												<a href="step1.php?tab=classified_text&id=2" style="background: url('assets/images/Recruitments.jpg') no-repeat scroll left center;"  aria-controls="where" id="MATR-MAIN" >
												<img src="assets/images/Recruitments.jpg" style="display:none">
												<span class="imgspan">Recruitments</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="step1.php?tab=classified_text&id=3" style="background: url('assets/images/Property-for-sale.jpg') no-repeat scroll left center;
												   " aria-controls="where"  id="MATR-MAIN">
												<img src="assets/images/Property-for-sale.jpg" style="display:none">
												<span class="imgspan">Property For Sale</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="step1.php?tab=classified_text&id=4" style="background: url('assets/images/Property-to-rent.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Property-to-rent.jpg" style="display:none">
												<span class="imgspan">Property To Rent</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									<div class="row">
									<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
									  </div>
										
									  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="step1.php?tab=classified_text&id=5" style="background: url('assets/images/Name-change.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Name-change.jpg" style="display:none">
												<span class="imgspan">Name Change</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="step1.php?tab=classified_text&id=6" style="background: url('assets/images/Lost-Found.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Lost-Found.jpg" style="display:none">
												<span class="imgspan">Lost Found</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="step1.php?tab=classified_text&id=7" style="background: url('assets/images/Vehicles.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Vehicles.jpg" style="display:none">
												<span class="imgspan">Vehicles</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="step1.php?tab=classified_text&id=8" style="background: url('assets/images/Astrology.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Astrology.jpg" style="display:none">
												<span class="imgspan">Astrology</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									
									
									<div class="row">
								<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
								  </div>
									
								  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="step1.php?tab=classified_text&id=9" style="background: url('assets/images/Business.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Business.jpg" style="display:none">
												<span class="imgspan">Business</span>
												</a>
											
											</div>
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="step1.php?tab=classified_text&id=10" style="background: url('assets/images/Computers.jpg') no-repeat scroll left center;
												  " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Computers.jpg" style="display:none">
												<span class="imgspan">Computers</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="step1.php?tab=classified_text&id=11" style="background: url('assets/images/Classified-Remembrance.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Classified-Remembrance.jpg" style="display:none">
												<span class="imgspan">Classified Remembrance</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Education.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Education.jpg" style="display:none">
												<span class="imgspan">Education</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									
									
									
									<div class="row">
									<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
									  </div>
										
									  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Obituary.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Obituary.jpg" style="display:none">
												<span class="imgspan">Obituary</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Personal-Announcement.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Personal-Announcement.jpg" style="display:none">
												<span class="imgspan">Personal Announcement</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Personal-Messages.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Personal-Messages.jpg" style="display:none">
												<span class="imgspan">Personal Messages</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Retail.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Retail.jpg" style="display:none">
												<span class="imgspan">Retail</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									
									<div class="row">
								<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
								  </div>
									
								  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Services.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Services.jpg" style="display:none">
												<span class="imgspan">Services</span>
												</a>
											
											</div>
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Travel.jpg') no-repeat scroll left center;
												  " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Travel.jpg" style="display:none">
												<span class="imgspan">Travel</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									
									
									</div>
									
                               </div>
							   <!--classified display ad -->
                          <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
            					<div class="row">
					<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
					  </div>
                      	
                      <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="step1.php?tab=classified_display&id=1" style="background: url('assets/images/Matrimonial.jpg') no-repeat scroll left center;
													" id="MATR-MAIN">
												<img src="assets/images/Matrimonial.jpg" style="display:none">
												<span class="imgspan">Matrimonial</span>
												</a>
											
											</div>
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="step1.php?tab=classified_display&id=2" style="background: url('assets/images/Recruitments.jpg') no-repeat scroll left center;" aria-controls="where" id="MATR-MAIN">
												<img src="assets/images/Recruitments.jpg" style="display:none">
												<span class="imgspan">Recruitments</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Property-for-sale.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Property-for-sale.jpg" style="display:none">
												<span class="imgspan">Property For Sale</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Property-to-rent.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Property-to-rent.jpg" style="display:none">
												<span class="imgspan">Property To Rent</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									<div class="row">
									<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
									  </div>
										
									  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Name-change.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Name-change.jpg" style="display:none">
												<span class="imgspan">Name Change</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Lost-Found.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Lost-Found.jpg" style="display:none">
												<span class="imgspan">Lost Found</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Vehicles.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Vehicles.jpg" style="display:none">
												<span class="imgspan">Vehicles</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Astrology.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Astrology.jpg" style="display:none">
												<span class="imgspan">Astrology</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									
									
									<div class="row">
								<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
								  </div>
									
								  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Business.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Business.jpg" style="display:none">
												<span class="imgspan">Business</span>
												</a>
											
											</div>
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Computers.jpg') no-repeat scroll left center;
												  " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Computers.jpg" style="display:none">
												<span class="imgspan">Computers</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Classified-Remembrance.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Classified-Remembrance.jpg" style="display:none">
												<span class="imgspan">Classified Remembrance</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Education.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Education.jpg" style="display:none">
												<span class="imgspan">Education</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									
									
									
									<div class="row">
									<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
									  </div>
										
									  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Obituary.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Obituary.jpg" style="display:none">
												<span class="imgspan">Obituary</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Personal-Announcement.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Personal-Announcement.jpg" style="display:none">
												<span class="imgspan">Personal Announcement</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Personal-Messages.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Personal-Messages.jpg" style="display:none">
												<span class="imgspan">Personal Messages</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Retail.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Retail.jpg" style="display:none">
												<span class="imgspan">Retail</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									
									<div class="row">
								<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
								  </div>
									
								  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Services.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Services.jpg" style="display:none">
												<span class="imgspan">Services</span>
												</a>
											
											</div>
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Travel.jpg') no-repeat scroll left center;
												  " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Travel.jpg" style="display:none">
												<span class="imgspan">Travel</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									
									
									</div>
									</div>
								  
								  </div>
                
               
								</div>
							</div>
						

					
					
					</div>
				</div>
			</div>

			<!-- end:: Body -->

		<?php include 'footer.php'; ?>