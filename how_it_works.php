<?PHP
 include 'header.php';
?>

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" style="padding-left: 0px">

				
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
				<h2 class=" m--margin-top-30 " style="text-align:center;">How it Works?</h2>
				<h4 class="sub_title">Built with a focus on simplicity and speed, the newspaperads.in design and booking platform is very easy to use. Follow the steps below to place your first newspaper ad, or watch the video.</h4>
				<div class="row side_padng">
				
				  <div class="col-md-4">
					<div class="m-content cont_padng">
                      <div class="m-portlet stat_page">
						<div class="m-portlet__body news_height1">
						<h5 class="red center">Step 1</h5>
						<h6 class="txt_news red m--margin-top-20">Choose ad type, newspaper, edition and dates.</h6>
<p class="li_para">a. Select the type of ad you want to book Classified Text Ad or Classified Display Ad.</p>
<p class="li_para">b. Choose from the listed categories (Matrimonial, Property for Sale etc.). This opens a new page. </p>
<p class="li_para">c. Select the publication (newspaper) that you want to place your ad in.</p>
<p class="li_para">d. Select the city. </p>
<p class="li_para">e. Select the publish (run) date and hit submit. This is the date(s) the ad will appear in the newspaper.</p>
<p class="li_para">f. Step 1 is complete. You have the option of changing the newspapers that this ad will appear in, including adding new ones.</p>
							</div>
							
                           </div>
					</div>
					</div>
					 <div class="col-md-4">
					<div class="m-content cont_padng">
                      <div class="m-portlet stat_page">
						<div class="m-portlet__body news_height1">
						<h5 class="red center">Step 2</h5>
						<h6 class="txt_news red m--margin-top-20"> Design you newspaper ad</h6>
<p class="li_para">a. There are 3 tabs titled Design Your Ad, Ad Enhancements, Ad Preview.</p>
<p class="li_para">b. Copy/Content for the ad goes into the Design Your Ad tab. (e.g. Property for Sale, 3 BHK, 1800 sq.ft. apt. on 3rd floor…). The number of lines and characters are automatically calculated and displayed.</p>
<p class="li_para">c. You can choose to add enhancements (background color or tick mark) to the ad to make it stand out. Additional charges apply. You can also choose from a range of pre-created ads, by clicking on Choose template.</p>
<p class="li_para">d. The Ad preview tab displays a preview of the ad that will be published. Make sure you edit the breakup and line spacing as required before proceeding to the next step.</p>
<p class="li_para">e. The total cost displayed at the end of the page. This automatically changes as you edit the content. You can choose to view the breakup of costs and click next to view the booking summary.</p>
				            </div>
							</div>
                           </div>
					</div>
					
					<div class="col-md-4">
					<div class="m-content cont_padng">
                      <div class="m-portlet stat_page">
						<div class="m-portlet__body news_height1">
						<h5 class="red center">Step 3</h5>
						<h6 class="txt_news red m--margin-top-20"> Design you newspaper ad</h6>
<p class="li_para">a. This page gives you a summary of the your order. Ad type, category, publication, city/edition, publish date, and cost is provided.</p>
<p class="li_para">b. Click on make payment. You will be required to create an account before payment can be processed and ad is booked. This is done for security purposes, to track orders, and request refunds.</p>
					       </div>
							</div>
                           </div>
					</div>
				 </div>
				</div>
			</div>

			<!-- end:: Body -->

<?PHP
 include 'footer.php';
?>