<?php
include 'database.php';
include 'header.php';
?>


			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" style="padding-left: 0px">

				
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					
					<div class="m-content">

						
						<!--Begin::Section-->
						<div class="m-portlet">
							
								<div class="m-portlet__body">
														<h4 class="m-portlet__head-text m-stack__item--center" style="text-align:center;">Billing Information</h4>
									<hr>
								
									
									<form name="billing-form" id="billing-form" action="/billingdetails" method="post" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
													
													        <div class="form-group m-form__group row">
																<label class="col-lg-2 col-form-label">Name<span class="red">*</span>:</label>
																<div class="col-lg-4">
																	<input type="text" name="addressLine1" id="addressLine1" class="form-control m-input" placeholder="Enter Full Name" value="">
																</div>
																<label class="col-lg-2 col-form-label">Phone Number<span class="red">*</span>:</label>
																<div class="col-lg-4">
																	<input type="text" name="addressLine2" id="addressLine2" class="form-control m-input" placeholder="Enter Phone Number" value="">
																</div>
															</div>
															
															<div class="form-group m-form__group row">
																<label class="col-lg-2 col-form-label">Address Line 1<span class="red">*</span>:</label>
																<div class="col-lg-4">
																	<input type="text" name="addressLine1" id="addressLine1" class="form-control m-input" placeholder="Enter Address Line 1" value="">
																</div>
																<label class="col-lg-2 col-form-label">Address Line 2:</label>
																<div class="col-lg-4">
																	<input type="text" name="addressLine2" id="addressLine2" class="form-control m-input" placeholder="Enter Address Line 2" value="">
																</div>
															</div>
															
															<div class="form-group m-form__group row">
																<label class="col-lg-2 col-form-label">City<span class="red">*</span>:</label>
																<div class="col-lg-4">
																	<input type="text" name="city" id="city" class="form-control m-input" placeholder="Enter City" value="">
																</div>
																<label class="col-lg-2 col-form-label">Postal Code<span class="red">*</span>:</label>
																<div class="col-lg-4">
																	<input type="text" name="postalCode" id="postalCode" class="form-control m-input" placeholder="Enter Postal Code" value="">
																</div>
															</div>
															
															
															<div class="form-group m-form__group row">
																<label class="col-lg-2 col-form-label">State<span class="red">*</span>:</label>
																<div class="col-lg-4">
																	<input type="text" name="state" id="state" class="form-control m-input" placeholder="Enter State" value="">
																</div>
																<label class="col-lg-2 col-form-label">Country<span class="red">*</span>:</label>
																<div class="col-lg-4">
																	<input type="text" name="country" id="country" class="form-control m-input" placeholder="Enter Country" value="">
																</div>
															</div>
															
															
															
															<div class="form-group m-form__group row">
																<label class="col-lg-2 col-form-label">GST Number:</label>
																<div class="col-lg-4 m--padding-bottom-10">
																	<input type="text" name="gstNo" id="gstNo" class="form-control m-input " placeholder="Enter GST Number" value="">
																</div>
																<div class="col-lg-2"></div>
																	<div class="col-lg-4 right">
																	<a href="/ads/design" class="btn m-btn--pill m-subheader-search__submit-btn  btn-secondary m--margin-right-10">Back</a>
																		<button type="submit" class="btn btn-brand">Submit</button>
																		
																	</div>
															</div>
															
														</div>
														
													</form>
										
									
									
									
									
									
										
								
								
							
						</div>
						
						
					
						
						</div>
						
					
					</div>

					</div>
				</div>
			</div>

			<!-- end:: Body -->

			<!-- begin::Footer -->
			
	
	
	
	
	
	
	
	
	
	
	
	<?php include 'footer.php'; ?>