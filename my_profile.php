<?php
include 'database.php';
include 'header.php';           
?> 
	<style>
.btn-profile{
    background-color: #00c5dc;
    border-radius: 4px;
    border: 1px solid #00c5dc;
    padding: 5px 15px 5px 15px;
    color: white;
    font-size: 16px;
	}
	</style>
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" style="padding-left: 0px">

				
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					
					<div class="m-content">

						
						<!--Begin::Section-->
						<div class="m-portlet">
							
								<div class="m-portlet__body">
														<h4 class="m-portlet__head-text m-stack__item--center" style="text-align:center;">Profile Information</h4>
									<hr>
									
									
										
				
			
									
									
									<div class="row  m--padding-top-20">

						<div class="col-xl-6 col-lg-6 col-md-12 ">	
                       <div class="row">						
							<div class="col-xl-4 col-lg-5 col-md-4 profile-userpic">		
					  <img  id="profile_img" class="img-responsive" src="assets/images/man-reading-a-newspaper_1325-404.jpg" alt="profile" width="190px" height="190px">
                          <div id="edit_placement"><a id="edit_pic" class="background-color-dark cursor-pointer"><i class="fas fa-pencil-alt"></i> Update Picture</a></div>					  
					  
					  </div>
					  <div class="col-xl-8 col-lg-7 col-md-6 m--padding-top-20">
							<h5 class="m-widget1__title">Name : <span class="m-widget1__desc">John doe </span> <a id="edit_name" class="cursor-pointer small font-color-grey"><i class="fas fa-pencil-alt"></i></a></h5>
							<div id="choose_name" class="m--padding-bottom-10 none">
										<form>
											<div class="btn-group" data-toggle="buttons">
												<input type="text" name="name"  class="form-control m-input" placeholder="Change Name" value="">
											</div>
											<button type="submit" class="btn btn-sm btn-red">Submit</button>						
										</form>
									</div>
						   <h5 class="m-widget1__title">E-Mail : <span class="m-widget1__desc">Johndoe@gmail.com </span><a id="edit_email" class="cursor-pointer small font-color-grey"><i class="fas fa-pencil-alt"></i></a></h5>
                                 <div id="choose_email" class="m--padding-bottom-10 none">
										<form>
											<div class="btn-group" data-toggle="buttons">
												<input type="text" name="name"  class="form-control m-input" placeholder="Change E-Mail " value="">
											</div>
											<button type="submit" class="btn btn-sm btn-red">Submit</button>						
										</form>
									</div>						   
                              <h5 class="m-widget1__title">Phone Number : <span class="m-widget1__desc">+91 9001234568 </span> <a id="edit_pnum" class="cursor-pointer small font-color-grey"><i class="fas fa-pencil-alt"></i></a></h5>	
							  <div id="choose_pnum" class="m--padding-bottom-10 none">
										<form>
											<div class="btn-group" data-toggle="buttons">
												<input type="text" name="name"  class="form-control m-input" placeholder="Change Phone Number" value="">
											</div>
											<button type="submit" class="btn btn-sm btn-red">Submit</button>						
										</form>
									</div>
                               <h5 class="m-widget1__title">Address : <span class="m-widget1__desc">L-12-20 Vertex, Cybersquare </span> <a id="edit_address" class="cursor-pointer small font-color-grey"><i class="fas fa-pencil-alt"></i></a></h5>	
                               <div id="choose_address" class="m--padding-bottom-10 none">
										<form>
											<div class="btn-group" data-toggle="buttons">
												<input type="text" name="name"  class="form-control m-input" placeholder="Change Address" value="">
											</div>
											<button type="submit" class="btn btn-sm btn-red">Submit</button>						
										</form>
									</div>							   
                         </div>
						 </div>
						
                          </div>							
													<div class="col-xl-6 col-lg-6 col-md-12  m--padding-top-10">
													
													<div class="row m--padding-top-20 m--padding-bottom-20">
													<div class="col-xl-6 col-lg-6 center m--padding-bottom-10 m--padding-left-20">
														<button class="btn-profile "><span class="fa-passwd-reset fa-stack">
														<i class="fa fa-credit-card fa-2x"></i>
                                                         </span><a  class="cursor-pointer m--padding-left-10">Billing Information </a></button>
														 </div>
														 <div class="col-xl-6 col-lg-6 center m--padding-bottom-10 m--padding-right-20">
														<button class="btn-profile"><span class="fa-passwd-reset fa-stack">
                                                          <i class="fa fa-undo fa-stack-2x"></i>
                                                          <i class="fa fa-lock fa-stack-1x"></i>
                                                         </span><a id="password" class="cursor-pointer "> Change Password</a>
														 </button>
														
													</div>
													
												</div>
												</div>
												</div>
												
									<div class="row margin-top-10 m--padding-bottom-30">
					<div class="col-xl-6 col-lg-6 col-md-12 m--padding-bottom-10">
					<div class="m--padding-top-30 m--padding-bottom-30 none border center" id="show_pic">	

							 
								<p class="m--padding-bottom-10 center">Upload New Profile Picture<br>
									<small class="text-grey center">(Format Allowed jpeg only, file size allowed maximum 500x500 pixel)</small>
								</p>							
								<form method="POST" enctype="multipart/form-data">
									<input type="file" accept="image/jpeg" required="" id="choose"> 
									<button type="submit" class="btn btn-xs btn-red">Submit</button>
								</form>	
							 
                             </div>	
					
					</div>
					<div class="col-xl-6 col-lg-6 col-md-12 m--padding-bottom-10">
					<div class="m--padding-top-10 m--padding-bottom-10 border none" id="show_password">	

								 <form method="post">
									<div class="row m--margin-top-10">
										<div class="col-md-4 m--margin-bottom-10">
											<p class="m--margin-top-5">Current Password <span class="text-red">*</span></p>
											<input type="password" name="advertiser_name" class="form-control input-sm " required="" id="password_focus">
										</div>
										<div class="col-md-4 m--margin-bottom-10">
											<p class="m--margin-top-5">New Password <span class="text-red">*</span></p>
											<input type="password" name="advertiser_name" class="form-control input-sm " required="">
										</div>
										<div class="col-md-4 m--margin-bottom-10">
											<p class="m--margin-top-5">Re-Type Password <span class="text-red">*</span></p>
											<input type="password" name="advertiser_name" class="form-control input-sm " required="">
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12 text-right m--padding-top-5 m--padding-bottom-5">
										  <button type="submit" class="btn btn-xs btn-red">Submit</button>
										  </div>
									</div>
								</form>     
							  </div>
					
					</div>
							
                           			
								
                             </div>					 
											
							
					  </div>
					   </div>
							
								
						</div>
						</div>

					</div>
				</div>
			</div>

			<!-- end:: Body -->

			<!-- begin::Footer -->
			
	
	
	
	
	
	
	
	
	
	
	
	

		<script>
			 $(document).ready(function(){
			
			  $("#show_password").hide();
			  $("#show_pic").hide();
			  $('#edit_pic').hide();
			  $('#choose_name').hide();
			  $('#choose_pnum').hide();
			  $("#choose_email").hide(); 
			  $("#choose_address").hide(); 
			  $('#edit_placement').hide();
			  
			 $("#edit_name").click(function(){
				$("#choose_name").toggle();  
				
			 });
			 $("#edit_pnum").click(function(){
				$("#choose_pnum").toggle();  
				
			 });
			  $("#edit_email").click(function(){
				$("#choose_email").toggle();  
				
			 });
			  $("#edit_address").click(function(){
				$("#choose_address").toggle();  
				
			 });
			 
			 $('#profile_img').hover(function(){
				$('#edit_pic').show();
				$('#edit_placement').show();
				
			 });
			 	
			  $("#edit_pic").click(function(){
				$("#show_pic").show();  	
				$("#choose").focus();
			  }); 		
 			$("#password").click(function(){
				$("#show_password").toggle();
				$("#password_focus").focus();
			  });

			   
			 });
		 </script>
	<?php include 'footer.php'; ?>