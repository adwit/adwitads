<?php


require_once "config.php";
// Define variables and initialize with empty values
$email = $firstname = $lastname =$company = $phone = $gst = $password = $confirm_password = "";
$email_err = $firstname_err =  $password_err = $confirm_password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
	

	
  
   $firstname = $_POST['firstname'];
  $lastname = $_POST['lastname'];
	  $company = $_POST['company'];
	    $phone = $_POST['phone'];
		  $gst = $_POST['gst'];
    // Validate email
    if(empty(trim($_POST["email"]))){
        $email_err = "Please enter a email.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM user_details WHERE email = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_email);
            
            // Set parameters
            $param_email = trim($_POST["email"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $email_err = "This Email is already exist.";
                } else{
                    $email = trim($_POST["email"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm password.";     
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }
    
    // Check input errors before inserting in database
    if(empty($email_err) && empty($firstname_err) && empty($password_err) && empty($confirm_password_err)){
        
        // Prepare an insert statement
        $sql = "INSERT INTO user_details (email,userfname,userlname,company,gst,phone, password) VALUES (?,?,?,?,?,?, ?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sssssss", $param_email,$param_firstname,$param_lastname,$param_company,$param_gst,$param_phone,$param_password);
            
            // Set parameters
            $param_email = $email;
			$param_firstname = $firstname;
			$param_lastname = $lastname;
			$param_company = $company;
			$param_gst = $gst;
			$param_phone = $phone;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
				 header("location: login.php");
              
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
include 'header.php';
?>	
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" style="padding-left: 0px">

				
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					
					<div class="m-content">

						
						<!--Begin::Section-->
						<div class="m-portlet">
							<div class="m-portlet__body">
							<h4 class="m-portlet__head-text m-stack__item--center" style="text-align:center;">Enter your details to create an account</h4>
											<hr>
											<head>
										
								
							<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">		
								<div class="row m-row--no-paddin">
									
									<div class="col-md-3"></div>
										<div class="col-xl-6 col-xs-12 col-md-6 col-sm-6">
	                                    
										 <div class="row">
										
										   <div class="col-xl-6 col-xs-12 col-md-6 col-sm-6 m--padding-bottom-10 <?php echo (!empty($firstname_err)) ? 'has-error' : ''; ?>">
										   <input type="text" class="form-control " placeholder="First name" name="firstname" value="<?php echo $firstname; ?>"/>
										     <span class="help-block"><?php echo $firstname_err; ?></span>
										   </div>
										   <div class="col-xl-6 col-xs-12 col-md-6 col-sm-6 m--padding-bottom-10">
										   <input type="text" class="form-control " placeholder="Last name" name="lastname" value="<?php echo $lastname; ?>"/>

										   </div>
										 </div>
										
											 <div class="row">
										   <div class="col-xl-6 col-xs-12 col-md-6 col-sm-6 m--padding-bottom-10  <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
										   <input type="email" class="form-control " placeholder="Email address" name="email" value="<?php echo $email; ?>"/>
										   <span class="help-block"><?php echo $email_err; ?></span>
										   </div>
										   <div class="col-xl-6 col-xs-12 col-md-6 col-sm-6 m--padding-bottom-10">
										   <input type="number" class="form-control" placeholder="Phone number" name="phone" value="<?php echo $phone; ?>"/>
										   </div>
										 </div>
										 
											 <div class="row">
										   <div class="col-xl-6 col-xs-12 col-md-6 col-sm-6 m--padding-bottom-10">
										   <input type="text" class="form-control " placeholder="Company name" name="company" value="<?php echo $company; ?>"/>
										   </div>
										   <div class="col-xl-6 col-xs-12 col-md-6 col-sm-6 m--padding-bottom-10">
										   <input type="text" class="form-control" placeholder="GST number" name="gst" value="<?php echo $gst; ?>"/>
										   </div>
										 </div>
										  <div class="row">
										   <div class="col-xl-6 col-xs-12 col-md-6 col-sm-6 m--padding-bottom-10 <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
										  <input  id="password-field" type="password"  name="password" value="<?php echo $password; ?>" class="form-control  m--margin-bottom-10" placeholder="Enter Password"/>
										    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
											
												<span class="help-block"><?php echo $password_err; ?></span>
											
										   </div>
										   <div class="col-xl-6 col-xs-12 col-md-6 col-sm-6 m--padding-bottom-10 <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
										   <input  id="password-field1" type="password"  name="confirm_password" value="<?php echo $confirm_password; ?>" class="form-control  m--margin-bottom-10" placeholder="Re-enter Password"/>
										    <span toggle="#password-field1" class="fa fa-fw fa-eye field-icon toggle-password"></span>
											<span class="help-block"><?php echo $confirm_password_err; ?></span>
										   </div>
										 </div>
										 
										  <div class="row">
										 <div class="col-xl-12 col-xs-12 col-md-12 col-sm-12 m--align-right m--padding-bottom-10">
										 <button type="submit" class=" m--pull-right btn m-btn--pill m-subheader-search__submit-btn btn-green ">Submit</button>
										</div>
									</div>
										 
										</div>
									
										</form>
								<div class="col-md-12 ">
								<hr>
										<p class="heading">Already have an account? <a href="login.php">Log In</a> </p>
										
									</div>	
								</div>
							

							</div>
						</div>
						
						
					
						
						</div>
						
					
					</div>

					</div>
				</div>
			</div>

			<!-- end:: Body -->

			<!-- begin::Footer -->
			
	
	
	
	
	
	
	
	
	
	

<?php include 'footer.php'; ?>