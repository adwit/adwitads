<!DOCTYPE html>

<html lang="en"> 

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>AdsHub | Dashboard</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

		<!--end::Web font -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="assets/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors --> 

		<!--begin:: Global Optional Vendors -->
		<link href="assets/vendors/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles -->
		<link href="assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />
        <link href="assets/demo/base/style.css" rel="stylesheet" type="text/css" />
		<!--RTL version:<link href="assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Global Theme Styles -->
 	     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!--begin::Page Vendors Styles -->
		<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Page Vendors Styles -->
		<link rel="shortcut icon" href="assets/demo/media/img/logo/favicon.ico" />
		
<style>
.help-block{
	color:red;
}
</style>
	</head>

	<!-- end::Head -->


<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--fixed m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- BEGIN: Header -->
			<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop">

						<!-- BEGIN: Brand -->
						<div class=" m-brand  m-brand--skin-light " style="background: #fff">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo m--margin-left-10">
									<a href="index.php" class="m-brand__logo-wrapper">
										<img alt="" src="assets/images/logo-adshub.png" />
									</a>
 
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">


									<!-- BEGIN: Responsive Header Menu Toggler -->
									<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>

									<!-- END -->

									<!-- BEGIN: Topbar Toggler -->
									<!--<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>-->

									<!-- BEGIN: Topbar Toggler -->
								</div>
							</div>
						</div>

						<!-- END: Brand -->
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
							<div class="m-header__title">
								<h3 class="m-header__title-text">&nbsp;</h3>
							</div>

							<!-- BEGIN: Horizontal Menu -->
							<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn"><i class="fa fa-times" aria-hidden="true"></i></button>
							<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light ">
								<ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
								<li class="m-menu__item m-menu__item--active" m-menu-link-redirect="1" aria-haspopup="true"><a href="index.php" class="m-menu__link "><span class="m-menu__link-text">Home</span></a></li>
										<li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="who_we_are.php" class="m-menu__link "><span class="m-menu__link-text">About Us</span></a></li>
									<li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="how_it_works.php" class="m-menu__link "><span class="m-menu__link-text">How it works?</span></a></li>

									<li class="m-menu__item" m-menu-link-redirect="1" aria-haspopup="true"><a href="why_news_paper_ads.php" class="m-menu__link "><span class="m-menu__link-text">Why news paper ads?</span></a></li>
									
									
									<li class="m-menu__item" m-menu-link-redirect="1" aria-haspopup="true"><a href="contact_us.php" class="m-menu__link m--padding-right-10"><span class="m-menu__link-text">Contact us</span></a></li>
									<?PHP 

 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
  
}
									if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){	
									
									echo "<li class='m-menu__item' m-menu-link-redirect='1' aria-haspopup='true'><a href='logout.php' class='m-menu__link'><span class='m-menu__link-text'>Log Out</span></a></li>";
									} else{
											echo "<li class='m-menu__item' m-menu-link-redirect='1' aria-haspopup='true'><a href='signup.php' class='m-menu__link'><span class='m-menu__link-text'>Sing Up</span></a></li>";
										echo "<li class='m-menu__item' m-menu-link-redirect='1' aria-haspopup='true'><a href='login.php' class='m-menu__link '><span class='m-menu__link-text'>Log in</span></a></li>";
								
									}
									
									?>
								</ul>
							</div>

							<!-- END: Horizontal Menu -->

							<!-- BEGIN: Topbar -->
							
							<!-- END: Topbar -->
						</div>
					</div>
				</div>
			</header>

			<!-- END: Header -->