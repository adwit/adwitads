<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
?>
<!DOCTYPE html>

<html lang="en"> 

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>AdsHub | Dashboard</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

		<!--end::Web font -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="assets/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors --> 

		<!--begin:: Global Optional Vendors -->
		<link href="assets/vendors/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="assets/vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles -->
		<link href="assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />
        <link href="assets/demo/base/style.css" rel="stylesheet" type="text/css" />
		<!--RTL version:<link href="assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Global Theme Styles -->
 	     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!--begin::Page Vendors Styles -->
		<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Page Vendors Styles -->
		<link rel="shortcut icon" href="assets/demo/media/img/logo/favicon.ico" />
		
<style>
.help-block{
	color:red;
}
</style>
	</head>

	<!-- end::Head -->


<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--fixed m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- BEGIN: Header -->
			<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop">

						<!-- BEGIN: Brand -->
						<div class=" m-brand  m-brand--skin-light " style="background: #fff">
							<div class="m-stack m-stack--ver m-stack--general">
								<div class="m-stack__item m-stack__item--middle m-brand__logo m--margin-left-10">
									<a href="index.php" class="m-brand__logo-wrapper">
										<img alt="" src="assets/images/logo-adshub.png" />
									</a>
 
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">


									<!-- BEGIN: Responsive Header Menu Toggler -->
									<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a>

									<!-- END -->

									<!-- BEGIN: Topbar Toggler -->
									<!--<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more"></i>
									</a>-->

									<!-- BEGIN: Topbar Toggler -->
								</div>
							</div>
						</div>

						<!-- END: Brand -->
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
							<div class="m-header__title">
								<h3 class="m-header__title-text">&nbsp;</h3>
							</div>

							<!-- BEGIN: Horizontal Menu -->
							<button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-light " id="m_aside_header_menu_mobile_close_btn"><i class="fa fa-times" aria-hidden="true"></i></button>
							<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light ">
								<ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
								<li class="m-menu__item m-menu__item--active" m-menu-link-redirect="1" aria-haspopup="true"><a href="index.php" class="m-menu__link "><span class="m-menu__link-text">Home</span></a></li>
										<li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="who_we_are.php" class="m-menu__link "><span class="m-menu__link-text">About Us</span></a></li>
									<li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="how_it_works.php" class="m-menu__link "><span class="m-menu__link-text">How it works?</span></a></li>

									<li class="m-menu__item" m-menu-link-redirect="1" aria-haspopup="true"><a href="why_news_paper_ads.php" class="m-menu__link "><span class="m-menu__link-text">Why news paper ads?</span></a></li>
									
									
									<li class="m-menu__item" m-menu-link-redirect="1" aria-haspopup="true"><a href="contact_us.php" class="m-menu__link m--padding-right-10"><span class="m-menu__link-text">Contact us</span></a></li>
										
										<li class="m-menu__item" m-menu-link-redirect="1" aria-haspopup="true"><a href="logout.php" class="m-menu__link "><span class="m-menu__link-text">Log Out</span></a></li>
								</ul>
							</div>

							<!-- END: Horizontal Menu -->

							<!-- BEGIN: Topbar -->
							
							<!-- END: Topbar -->
						</div>
					</div>
				</div>
			</header>

			<!-- END: Header -->
      		<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" style="padding-left: 0px">

				
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<div class="m-subheader-search">
						<div class="row">
						
						<div class="col-xl-8 col-md-8 col-sm-12 col-xs-12 " style="    margin-top: 80px;    padding-left: 0px;">
								<h1 class="m-subheader-search__title" >Book classified & display advertisements</br> across leading newspapers in 3 easy steps</h1>
						
						</div>
						<div class="col-md-4">
							<img src="assets/images/reading-newspaper.png" style="width: 100%;">
						</div>
						</div>
					</div>
					
					<div class="m-content">

						<div class="m-portlet">
						<div class="m-portlet__body">
							
								<div class="row m--margin-bottom-30 m--margin-top-30">
									<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
					               </div>
								<div class="col-xl-8 col-md-12 col-sm-12 col-xs-12">
									<nav>
                                       <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
					                            <h4 class="m-widget14__title ">
													Classified text ad
												</h4>
												<h6 class="m-widget14__title titlesml" >Click below category</h6>
												
											 </a>
                                             <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
												<h4 class="m-widget14__title" >
													Classified display ad
												</h4>
												<h6 class="m-widget14__title titlesml" >Click below category</h6>
												
											  </a>
									      </div>
									  </nav>
									</div>
								</div>
				
				
                  
                  <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
					<div class="row">
					<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
					  </div>
                      	
                      <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="step1.php?tab=classified_text&id=1" style="background: url('assets/images/Matrimonial.jpg') no-repeat scroll left center;
													" aria-controls="where"   id="MATR-MAIN">
												<img src="assets/images/Matrimonial.jpg" style="display:none">
												<span class="imgspan"> Matrimonial </span>
												</a>
											
											</div>
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
											
												<a href="step1.php?tab=classified_text&id=2" style="background: url('assets/images/Recruitments.jpg') no-repeat scroll left center;"  aria-controls="where" id="MATR-MAIN" >
												<img src="assets/images/Recruitments.jpg" style="display:none">
												<span class="imgspan">Recruitments</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Property-for-sale.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Property-for-sale.jpg" style="display:none">
												<span class="imgspan">Property For Sale</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Property-to-rent.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Property-to-rent.jpg" style="display:none">
												<span class="imgspan">Property To Rent</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									<div class="row">
									<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
									  </div>
										
									  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Name-change.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Name-change.jpg" style="display:none">
												<span class="imgspan">Name Change</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Lost-Found.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Lost-Found.jpg" style="display:none">
												<span class="imgspan">Lost Found</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Vehicles.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Vehicles.jpg" style="display:none">
												<span class="imgspan">Vehicles</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Astrology.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Astrology.jpg" style="display:none">
												<span class="imgspan">Astrology</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									
									
									<div class="row">
								<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
								  </div>
									
								  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Business.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Business.jpg" style="display:none">
												<span class="imgspan">Business</span>
												</a>
											
											</div>
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Computers.jpg') no-repeat scroll left center;
												  " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Computers.jpg" style="display:none">
												<span class="imgspan">Computers</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Classified-Remembrance.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Classified-Remembrance.jpg" style="display:none">
												<span class="imgspan">Classified Remembrance</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Education.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Education.jpg" style="display:none">
												<span class="imgspan">Education</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									
									
									
									<div class="row">
									<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
									  </div>
										
									  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Obituary.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Obituary.jpg" style="display:none">
												<span class="imgspan">Obituary</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Personal-Announcement.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Personal-Announcement.jpg" style="display:none">
												<span class="imgspan">Personal Announcement</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Personal-Messages.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Personal-Messages.jpg" style="display:none">
												<span class="imgspan">Personal Messages</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Retail.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Retail.jpg" style="display:none">
												<span class="imgspan">Retail</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									
									<div class="row">
								<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
								  </div>
									
								  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Services.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Services.jpg" style="display:none">
												<span class="imgspan">Services</span>
												</a>
											
											</div>
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Travel.jpg') no-repeat scroll left center;
												  " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Travel.jpg" style="display:none">
												<span class="imgspan">Travel</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									
									
									</div>
									
                               </div>
							   <!--classified display ad -->
                          <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
            					<div class="row">
					<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
					  </div>
                      	
                      <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="step1.php?tab=classified_display&id=1" style="background: url('assets/images/Matrimonial.jpg') no-repeat scroll left center;
													" id="MATR-MAIN">
												<img src="assets/images/Matrimonial.jpg" style="display:none">
												<span class="imgspan">Matrimonial</span>
												</a>
											
											</div>
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="step1.php?tab=classified_display&id=2" style="background: url('assets/images/Recruitments.jpg') no-repeat scroll left center;" aria-controls="where" id="MATR-MAIN">
												<img src="assets/images/Recruitments.jpg" style="display:none">
												<span class="imgspan">Recruitments</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Property-for-sale.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Property-for-sale.jpg" style="display:none">
												<span class="imgspan">Property For Sale</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Property-to-rent.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Property-to-rent.jpg" style="display:none">
												<span class="imgspan">Property To Rent</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									<div class="row">
									<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
									  </div>
										
									  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Name-change.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Name-change.jpg" style="display:none">
												<span class="imgspan">Name Change</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Lost-Found.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Lost-Found.jpg" style="display:none">
												<span class="imgspan">Lost Found</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Vehicles.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Vehicles.jpg" style="display:none">
												<span class="imgspan">Vehicles</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Astrology.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Astrology.jpg" style="display:none">
												<span class="imgspan">Astrology</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									
									
									<div class="row">
								<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
								  </div>
									
								  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Business.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Business.jpg" style="display:none">
												<span class="imgspan">Business</span>
												</a>
											
											</div>
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Computers.jpg') no-repeat scroll left center;
												  " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Computers.jpg" style="display:none">
												<span class="imgspan">Computers</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Classified-Remembrance.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Classified-Remembrance.jpg" style="display:none">
												<span class="imgspan">Classified Remembrance</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Education.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Education.jpg" style="display:none">
												<span class="imgspan">Education</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									
									
									
									<div class="row">
									<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
									  </div>
										
									  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Obituary.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Obituary.jpg" style="display:none">
												<span class="imgspan">Obituary</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Personal-Announcement.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Personal-Announcement.jpg" style="display:none">
												<span class="imgspan">Personal Announcement</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Personal-Messages.jpg') no-repeat scroll left center;
													" aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Personal-Messages.jpg" style="display:none">
												<span class="imgspan">Personal Messages</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Retail.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Retail.jpg" style="display:none">
												<span class="imgspan">Retail</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									</div>
									
									<div class="row">
								<div class="col-xl-2 col-xs-0 col-md-0 col-sm-0">
								  </div>
									
								  <div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Daily Sales-->
										<div class="borderline">
											<div class="m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Services.jpg') no-repeat scroll left center;
												   " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Services.jpg" style="display:none">
												<span class="imgspan">Services</span>
												</a>
											
											</div>
										</div>

										<!--end:: Widgets/Daily Sales-->
									</div>
									<div class="col-xl-2 col-xs-12 col-md-3 col-sm-12">

										<!--begin:: Widgets/Stats2-1 -->
										<div class="borderline">
											<div class=" m--margin-bottom-30">
												<a href="#where" style="background: url('assets/images/Travel.jpg') no-repeat scroll left center;
												  " aria-controls="where" role="tab" data-toggle="tab" id="MATR-MAIN">
												<img src="assets/images/Travel.jpg" style="display:none">
												<span class="imgspan">Travel</span>
												</a>
		
											</div>
											
										</div>

										<!--end:: Widgets/Stats2-1 -->
									</div>
									
									
									</div>
									</div>
								  
								  </div>
                
               
								</div>
							</div>
						

					
					
					</div>
				</div>
			</div>

			<!-- end:: Body -->

		<?php include 'footer.php'; ?>