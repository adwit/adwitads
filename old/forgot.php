<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect to login page
//if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
 //   header("location: login.php");
//    exit;
//}
 
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$new_password = $confirm_password = "";
$new_password_err = $confirm_password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Validate new password
    if(empty(trim($_POST["new_password"]))){
        $new_password_err = "Please enter the new password.";     
    } elseif(strlen(trim($_POST["new_password"])) < 6){
        $new_password_err = "Password must have atleast 6 characters.";
    } else{
        $new_password = trim($_POST["new_password"]);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm the password.";
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($new_password_err) && ($new_password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }
        
    // Check input errors before updating the database
    if(empty($new_password_err) && empty($confirm_password_err)){
        // Prepare an update statement
        $sql = "UPDATE users SET password = ? WHERE id = ?";
        
        if($stmt = mysqli_prepare($conn, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "si", $param_password, $param_id);
            
            // Set parameters
            $param_password = password_hash($new_password, PASSWORD_DEFAULT);
            $param_id = $_SESSION["id"];
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Password updated successfully. Destroy the session, and redirect to login page
                session_destroy();
                header("location: login.php");
                exit();
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($conn);
}


include 'header.php';
?>
 
			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" style="padding-left: 0px">

				
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					
					<div class="m-content">

						
						<!--Begin::Section-->
						<div class="m-portlet">
							<div class="m-portlet__body">
							<h4 class="m-portlet__head-text m-stack__item--center" style="text-align:center;">Please reset your account password. </h4>
											<hr>
								<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"> 
								<div class="row m-row--no-padding m--padding-bottom-30">
										<div class="col-xl-5 col-xs-12 col-md-4 col-sm-5"></div>
										<div class="col-xl-3 col-xs-12 col-md-5 col-sm-5">
	                                   <div class="row m--margin-right-30">
										   <div class="col-xl-10 col-xs-12 col-md-12 col-sm-12 m--padding-bottom-10 <?php echo (!empty($new_password_err)) ? 'has-error' : ''; ?>">
										   <input  id="password-field" type="password"  name="new_password"  class="form-control  m--margin-bottom-10" placeholder="New Password" value="<?php echo $new_password; ?>">
                                            <span class="help-block"><?php echo $new_password_err; ?></span>
										    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
											</div>
											</div>
											<div class="row m--margin-right-30">
										   <div class="col-xl-10 col-xs-12 col-md-12 col-sm-12 m--padding-bottom-10 <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
										   <input id="password-field1" type="password"  name="confirm_password" class="form-control m--margin-bottom-10" placeholder="Confirm Password"/>
										    <span class="help-block"><?php echo $confirm_password_err; ?></span>
										   <span toggle="#password-field1" class="fa fa-fw fa-eye field-icon toggle-password"></span>
										   </div>
										 </div>
										 <div class="row m--margin-right-30">
										 <div class="col-xl-10 col-xs-12 col-md-12 col-sm-12 m--align-right">
										<button type="submit" name="submit" class=" m--pull-right btn m-btn--pill m-subheader-search__submit-btn btn-green ">Submit</button>
										</div>
									</div>
									</div>	
									</div>
								</form>
							</div>
						</div>
						
						
					
						
						</div>
						
					
					</div>

					</div>
     
   <?php include 'footer.php'; ?>