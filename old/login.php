

<?php


// Initialize the session
session_start();
 
// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
    header("location: step3_payment.php");
    exit;
}
 
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$email = $password = "";
$email_err = $password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Check if email is empty
    if(empty(trim($_POST["email"]))){
        $email_err = "Please enter email.";
    } else{
        $email = trim($_POST["email"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter your password.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate credentials
    if(empty($email_err) && empty($password_err)){
        // Prepare a select statement
        $sql = "SELECT id, email, password FROM user_details WHERE email = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_email);
            
            // Set parameters
            $param_email = $email;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Store result
                mysqli_stmt_store_result($stmt);
                
                // Check if email exists, if yes then verify password
                if(mysqli_stmt_num_rows($stmt) == 1){                    
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $email, $hashed_password);
                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($password, $hashed_password)){
                            // Password is correct, so start a new session
                            session_start();
                            
                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["email"] = $email;                            
                            
                            // Redirect user to welcome page
                            header("location: step3_payment.php");
                        } else{
                            // Display an error message if password is not valid
                            $password_err = "The password you entered was not valid.";
                        }
                    }
                } else{
                    // Display an error message if email doesn't exist
                    $email_err = "No account found with that email.";
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}

include 'header.php';
?>

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" style="padding-left: 0px">

				
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					
					<div class="m-content">

						
						<!--Begin::Section-->
						<div class="m-portlet">
							<div class="m-portlet__body">
							<h5  style="color:red;text-align:center;"></h5>
							<h4 class="m-portlet__head-text m-stack__item--center" style="text-align:center;">Sign In to your Account</h4>
											<hr>
								  <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
								<div class="row m-row--no-padding ">
									
									<div class="col-md-4"></div>
										<div class="col-xl-4 col-xs-12 col-md-6 col-sm-6">
	                                   
										
											 
										<div class="row">
										<div class="col-xl-1 col-xs-0 col-md-0 col-sm-0 "></div>
										   <div class="col-xl-10 col-xs-12 col-md-10 col-sm-12 m--padding-bottom-10 <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
										   <input type="text" class="form-control  m--margin-bottom-10" name="email" value="<?php echo $email; ?>" placeholder="Email address" required/>
										   <span class="help-block"><?php echo $email_err; ?></span>
										 </div>
										 </div>
										 <div class="row">
										<div class="col-xl-1 col-xs-0 col-md-0 col-sm-0 "></div>
										   <div class="col-xl-10 col-xs-12 col-md-10 col-sm-12 m--padding-bottom-10 <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
										    <input  id="password-field" type="password"  name="password"  class="form-control  m--margin-bottom-10" placeholder="Enter Password"/>
										    <span class="help-block"><?php echo $password_err; ?></span>
											<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
										   </div>
										 </div>
										 
										 <div class="row m-login__form-sub">
										 <div class="col-xl-1 col-xs-0 col-md-0 col-sm-0 "></div>
											<div class="col-xl-5 col-xs-12 col-md-5 col-sm-6">
												<label class="m-checkbox m-checkbox--focus">
													<input type="checkbox" name="remember"> Remember me
													<span></span>
												</label>
											</div>
											<div class="col-xl-5 col-xs-12 col-md-5 col-sm-6 m--align-right">
												<a href="forgot.php" id="m_login_forget_password" class="m-link">Forget Password ?</a>
											</div>
										</div>
										 <div class="row">
										 <div class="col-xl-11 col-xs-12 col-md-10 col-sm-10 m--align-right">
										 <button type="submit" class=" m--pull-right btn m-btn--pill m-subheader-search__submit-btn btn-green ">Sign In</button>
										</div>
									</div>
									</div>	
									
									
									
									<div class="col-md-12 "><hr>
										<p class="heading">Don't have an account yet ? <a href="signup.php">Sign Up</a> </p>
										
									</div>
									
										
								</div>
								</form>
								
							</div>
						</div>
						
						
					
						
						</div>
						
					
					</div>

					</div>
				</div>
			</div>

			<!-- end:: Body -->

			<!-- begin::Footer -->
			
	
	
	
	
	
	
	
	
	
	
	
	
<?php include 'footer.php'; ?>