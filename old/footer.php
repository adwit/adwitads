	<!-- begin::Footer -->
			


         <footer class="m-grid__item		m-footer " style="margin-left: 0px">
				<div class="m-container m-container--fluid m-container--full-height m-page__container">
					<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
						<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								2019 &copy; AdsHub
							</span>
						</div>
						<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
							<ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
								<li class="m-nav__item">
									<a href="#" class="m-nav__link">
										<span class="m-nav__link-text">About</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link">
										<span class="m-nav__link-text">Privacy</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link">
										<span class="m-nav__link-text">T&C</span>
									</a>
								</li>
								<li class="m-nav__item">
									<a href="#" class="m-nav__link">
										<span class="m-nav__link-text">Purchase</span>
									</a>
								</li>
								<li class="m-nav__item m-nav__item">
									<a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">
										<i class="fa fa-question-circle-o" style="font-size:24px; position:relative;right: 5px;top: 2px;color: #c1bfd0;" aria-hidden="true"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</footer>
			<!-- end::Footer -->
		</div>

		<!-- end:: Page -->

		

		<!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="fa fa-arrow-up" aria-hidden="true"></i>
		</div>

		<!-- end::Scroll Top -->

		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet"/>

	<!--begin:: Global Mandatory Vendors -->
		<script src="assets/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="assets/vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="assets/vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="assets/vendors/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="assets/vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="assets/vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="assets/vendors/wnumb/wNumb.js" type="text/javascript"></script>

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<script src="assets/vendors/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="assets/vendors/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="assets/vendors/jquery.repeater/src/repeater.js" type="text/javascript"></script>
		<script src="assets/vendors/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="assets/vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js" type="text/javascript"></script>
		<script src="assets/vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
		<script src="assets/vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
		<script src="assets/vendors/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
		<script src="assets/vendors/handlebars/dist/handlebars.js" type="text/javascript"></script>
		<script src="assets/vendors/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
		<script src="assets/vendors/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="assets/vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
		<script src="assets/vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js" type="text/javascript"></script>
		<script src="assets/vendors/nouislider/distribute/nouislider.js" type="text/javascript"></script>
		<script src="assets/vendors/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
		<script src="assets/vendors/autosize/dist/autosize.js" type="text/javascript"></script>
		<script src="assets/vendors/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
		<script src="assets/vendors/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
		<script src="assets/vendors/dropzone/dist/dropzone.js" type="text/javascript"></script>
		<script src="assets/vendors/summernote/dist/summernote.js" type="text/javascript"></script>
		<script src="assets/vendors/markdown/lib/markdown.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js" type="text/javascript"></script>
		<script src="assets/vendors/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
		<script src="assets/vendors/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
		<script src="assets/vendors/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script>
		<script src="assets/vendors/toastr/build/toastr.min.js" type="text/javascript"></script>
		<script src="assets/vendors/jstree/dist/jstree.js" type="text/javascript"></script>
		<script src="assets/vendors/raphael/raphael.js" type="text/javascript"></script>
		<script src="assets/vendors/morris.js/morris.js" type="text/javascript"></script>
		<script src="assets/vendors/chartist/dist/chartist.js" type="text/javascript"></script>
		<script src="assets/vendors/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
		<script src="assets/vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
		<script src="assets/vendors/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
		<script src="assets/vendors/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
		<script src="assets/vendors/counterup/jquery.counterup.js" type="text/javascript"></script>
		<script src="assets/vendors/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
		<script src="assets/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
		<script src="assets/vendors/js/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Bundle -->
		<script src="assets/demo/base/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors -->
		<script src="assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts -->
		<script src="assets/app/js/dashboard.js" type="text/javascript"></script>


<script src="assets/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
		<script src="assets/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
		<script src="assets/custom/crud/wizard/wizard.js" type="text/javascript"></script>
		
		<script>
		$('.date').datepicker({
    multidate: true
});



$(".button").on("click", function() {
  var modal = $(this).data("modal");
  $(modal).show();
});

$(".modal").on("click", function(e) {
  var className = e.target.className;
  if(className === "modal" || className === "close"){
    $(this).closest(".modal").hide();
  }
});


	$(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});

		
// Get the modal
var publicationModal = document.getElementById('myModal');
var costBreakUpModal = document.getElementById('modalTwo');
var resetUpModal = document.getElementById('resetModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");
var costBrkbtn = document.getElementById("costBreakUpBtn");



// Get the <span> element that closes the modal

// When the user clicks the button, open the modal 
if(btn!=null){
	btn.onclick = function() {
		publicationModal.style.display = "block";
	}
}


if(costBrkbtn!=null){
	costBrkbtn.onclick = function() {
		costBreakUpModal.style.display = "block";
	}
}






var span = document.getElementsByClassName("close")[0];
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  costBreakUpModal.style.display = "none";
}

var pSpan = document.getElementsByClassName("pClose")[0];
pSpan.onclick = function() {
	publicationModal.style.display = "none";
}

var rSpan = document.getElementsByClassName("rClose")[0];
rSpan.onclick = function() {
	resetUpModal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == publicationModal || event.target == costBreakUpModal) {
	  publicationModal.style.display = "none";
    costBreakUpModal.style.display = "none";
  }
}


		var $publication = $('#m_select_publication');
		var $cities = $('#m_select_city');

	    var $bgColor = $('input:radio[name=bgColor]');
	    
		
		var tickMark = '&#10004;&nbsp;';
		var basicCost = '4540.0';
		var basicServiceCost = '227.0';
		
		var wordsPerLine = '24';
		var pricePerLine = '908.0';
		
		var minChars = '120';
		var extraLines = 0;
		var totalLines = 5;
		var minLines = '5';
		var totalRunDates = '';
		
		
		$(document).ready(function() {
			resetForms();
			
			
			 
			  
			 $publication.select2({
		            placeholder: "Select Newspaper",
		        }).val([1]).trigger("change");
			 
			 $publication.change(function(event) {
				 var items= $(this).val();
		    	updateCityList(items.toString());
		    	  
		      });
			 
			 
			 $(".sampleTemplateId").on('click', function(event){
				    //console.log($(this).attr('data-bodyTextColor'));
					 
				    var selText = $(this).text().trim();
				     
				    if ($('#tick_mark').is(':checked')) {
			        	$("#adPreviewId").html(tickMark+selText);
			        }else{ 
			        	$("#adPreviewId").html(selText);
			        }
				     
				    $("#ad_text_compose").val(selText);
				   
				    var totalChar = selText.length;
				     
				    $('#char_entered_count').html(totalChar);
				    calCulateExtraLine(totalChar);
				    
				    $('#m_modal_2').modal('hide'); 
				});  
			 
			 
			
			 
    });
		
	
		function resetForms() {
		    document.forms['ad-design-from'].reset();
		}
		
	
		 $('#m_select_publication').change(function(event) {

	    	  var pubId= $(this).val();

	    	  console.log(pubId);
	    	  
	    	  if(pubId!=null){
	    		  var jsonEditionObject = []; 
	        	   
	       		  $.ajax({  
	    		       url : '/ads/geteditionlist',  
	    		       type : 'POST',
	    		       dataType: 'json',
	    		       data: { adCategoryId: '1', publicationId:pubId},
	    		       success : function(response) {  
	    		    	   jQuery(response).each(function(i, item){
	    		    		   console.log(item);

	    		    		   edition = {}
	    		    		    
	    		    		   //edition ["id"] = item.editionId;
	    		    		   edition ["id"] = item.city.cityId;
	    		    		   edition ["text"] = item.city.name;
	    		    		   jsonEditionObject.push(edition);
	    		    		});
	    		    	   
	      
	    		     		 
	    		     		if(jsonEditionObject.length>0){
	    		     				$cities.select2({
	    	                		  placeholder: "Select City",
	    	                		  data: jsonEditionObject
	    	                		});
	    		     		}
	    		     		
	    		     		
	    		     		  
	    		       } 
	    			}); 
	    	  }else{
	    		  $cities.html('').select2({
		     			allowClear: true,
		     			 placeholder: "Select City",
		     			data: {id:null, text: null}
		     		});
	    	  }
	    	  
	      });
		 
		 updateCityList = function(pubId){


	    	  console.log(pubId);
	    	  
	    	  if(pubId!=null){
	    		  var jsonEditionObject = []; 
	        	   
	       		  $.ajax({  
	    		       url : '/ads/geteditionlist',  
	    		       type : 'POST',
	    		       dataType: 'json',
	    		       data: { adCategoryId: '1', publicationId:pubId},
	    		       success : function(response) {  
	    		    	   jQuery(response).each(function(i, item){
	    		    		   console.log(item);

	    		    		   edition = {}
	    		    		    
	    		    		   //edition ["id"] = item.editionId;
	    		    		   edition ["id"] = item.city.cityId;
	    		    		   edition ["text"] = item.city.name;
	    		    		   jsonEditionObject.push(edition);
	    		    		});
	    		    	   
	      
	    		     		 
	    		     		if(jsonEditionObject.length>0){
	    		     				$cities.select2({
	    	                		  placeholder: "Select City",
	    	                		  data: jsonEditionObject
	    	                		});
	    		     		}
	    		     		
	    		     		
	    		     		  
	    		       } 
	    			}); 
	    	  }else{
	    		  $cities.html('').select2({
		     			allowClear: true,
		     			 placeholder: "Select City",
		     			data: {id:null, text: null}
		     		});
	    	  }
	    	  
	      };
		 
	      
	      
	      
		      $('#sel_date').datepicker({
					multidate: true,
					format: 'dd M',
					 orientation: "bottom right",
					startDate: '+1d',
					beforeShowDay: function(date) {
						if(date.getDay() == 0){
							return false;  
						}else{
							return true;
						} 
					}
					
		
				}).on("changeDate", function (e) {
					var selValues = $(this).val();
					console.log(selValues);
				   $("#selDate").text(selValues);
				   $('#sel_hdate').val(selValues);
				   $('#sel_date').val('');
				}); 

	      
	      
	      designAd = function(){
				var isFormValid = true;

				if($('#selDate').text().trim().length==0){
						  $('#selDateTxt').addClass('m--font-danger');
						isFormValid = false;
					}
				
				if($('#m_select_publication').val()===null){
					$('#help_text_publication').addClass('m--font-danger');
					isFormValid = false;
				}
				
				if($('#m_select_city').val()===null){
					$('#help_text_publication').addClass('m--font-danger');
					isFormValid = false;
				}
				 
				if(isFormValid){
					var ad_text_compose = $("#ad_text_compose").val();
					var tick_mark= $('#tick_mark').is(':checked')?1:0;
					var bgColor= $("input[name='bgColor']:checked").val();
					
					
					$('#sel_ad_text_compose').val(ad_text_compose);
					$('#sel_tick_mark').val(tick_mark);
					$('#sel_bgColor').val(bgColor);
					
					console.log('publish submit');
					$('#ads-publish-form').submit();
				}
			}
	      
	      
		
		updatePublicationList = function(cityIds){
			 var jsonPublicationObject = [];
			 
			$.ajax({  
  		       url : '/ads/getpublicationlist',  
  		       type : 'POST',
  		       dataType: 'json',
  		       data: { aCityList: cityIds},
  		       success : function(response) {  
  		    	   jQuery(response).each(function(i, item){
  		    		   
  		    		    publication = {}
  		    		    
  		    		    publication ["id"] = item.publicationId;
  		    		    publication ["text"] = item.name;
  		    	        jsonPublicationObject.push(publication);
  		    		});
  		    	   
 			     		$publication.html('').select2({
 			     			allowClear: true,
 			     			 placeholder: "Select Newspaper",
 			     			data: {id:null, text: null}
 			     		});
    
  		     		 
  		     		if(jsonPublicationObject.length>0){
  		     				$publication.select2({
  	                		  placeholder: "Select Newspaper",
  	                		  data: jsonPublicationObject
  	                		});
  		     		}
  		     		  
  		       } 
  			});
		}
		
		
		searchForm = function(){
			var isFormValid = true;
			
			if($('#m_select_city').val().length==0){
				$('#help_text_city').addClass('m--font-danger');
				isFormValid = false;
			}
			 
			if($('#m_select_publication').val()===null){
				$('#help_text_publication').addClass('m--font-danger');
				isFormValid = false;
			}
			
			if($('#m_select_type').val()===null){
				$('#help_text_type').addClass('m--font-danger');
				isFormValid = false;
			}
			
			if($('#m_select_category').val()===null){
				$('#help_text_category').addClass('m--font-danger');
				isFormValid = false;
			}
			
			if(isFormValid){
				$('#ads-search-form').submit();
			}
		}
		
		
		$("#ad_text_compose").on('change keyup paste', function() {
			var currentVal = $(this).val();
			 
			 var totalChar = currentVal.length;
			 $('#char_entered_count').html(totalChar);
			 
			 if ($('#tick_mark').is(':checked')) {
				 $("#adPreviewId").html(tickMark+currentVal);
			 }else{
				 $("#adPreviewId").html(currentVal);
			 }
			 
			 calCulateExtraLine(totalChar);
			 
		}); 
		

		$bgColor.change(function() {
			console.log(this.value);
			 $("#adPreviewId").css("background-color", this.value);
			 
			
			  if(this.value=='#ffffff'){
				  $('#price_bgColorId').addClass('m--hide');
			  }else{
				  $('#price_bgColorId').removeClass('m--hide');
			  }
			   
			  calculatePrice();
		});
		
		
$('#tick_mark').click(function() {
			
			var adText = $("#ad_text_compose").val();
			
	        if ($(this).is(':checked')) {
	        	$("#adPreviewId").html(tickMark+adText);
	        	$('#price_tickMarkId').removeClass('m--hide');
	        }else{
	        	$("#adPreviewId").html(adText);
	        	$('#price_tickMarkId').addClass('m--hide');
	        }
	        	
	        calculatePrice();
	    });
	    
		
		calCulateExtraLine = function(totalCharEntered){
			totalLines = Math.ceil(totalCharEntered / wordsPerLine);
			$('#line_entered_count').html(totalLines);
			
			if(totalCharEntered>minChars){
				$('#price_extraLineId').removeClass('m--hide');
			}else{
				extraLines = 0;
				$('#price_extraLineId').addClass('m--hide');
			}
			
			calculatePrice();
		}
		
		
		
		calculatePrice = function(){
			
			var extraLines = parseInt(totalLines)-parseInt(minLines);
			var extraLinePrice = 0;
			if(extraLines>0){
				extraLinePrice = extraLines*parseInt(pricePerLine);
			}
			
			$('#extraLinePriceId').html(extraLinePrice.toLocaleString());
			
			var tickMarkPrice = 0;
			if ($('#tick_mark').is(':checked')) {
				var tickMarkPercentage = parseInt($('#tickMarkPriceSubId').text());
				tickMarkPrice = (tickMarkPercentage/100)*(extraLinePrice+parseInt(basicCost));
				
				console.log('tickMarkPercentage : '+tickMarkPercentage);
				console.log('tickMarkPrice : '+tickMarkPrice);
				
				$('#tickMarkPriceId').html(tickMarkPrice.toLocaleString());
			}
			
			var bgPrice = 0;
			 var bgColorSelected = $("input[name='bgColor']:checked").val();
			  
			if(bgColorSelected!='#ffffff'){
				var bgPricePercentage = parseInt($('#bgColorPriceSubId').text());
				console.log('bgPricePercentage : '+bgPricePercentage);
				bgPrice = (bgPricePercentage/100)*(extraLinePrice+parseInt(basicCost));
				console.log('bgPricePercentage : '+bgPricePercentage);
				console.log('bgPrice : '+bgPrice);
				
				$('#bgColorPriceId').html(bgPrice.toLocaleString());
				
			}
			
			var serviceCost = parseInt(basicServiceCost)+parseInt(extraLinePrice*0.05);
			
			if ($('#tick_mark').is(':checked')) {
				console.log('tick price added : '+tickMarkPrice);
				serviceCost = serviceCost+parseInt(tickMarkPrice*0.05)
			}
			
			if(bgColorSelected!='#ffffff'){
				console.log('bg price added : '+bgPrice);
				serviceCost = serviceCost+parseInt(bgPrice*0.05)
			}
			
			$('#price_ServiceCharge').html(serviceCost.toLocaleString());
			 
			
			var totalPrice = parseInt(basicCost)+parseInt(extraLinePrice)+parseInt(serviceCost);
			if ($('#tick_mark').is(':checked')) {
				totalPrice = totalPrice+parseInt(tickMarkPrice);
			}
			
			
			if(bgColorSelected!='#ffffff'){
				totalPrice = totalPrice+parseInt(bgPrice);
			}
			 
			$('#totalCostPrice').html(totalPrice.toLocaleString());
			$('#totalAdCostId').html(totalPrice.toLocaleString());
			
		}
		
		
		publishDesign = function(){
			var adText = $("#ad_text_compose").val();
			if(adText==""){
				$("#ad_text_compose").addClass('redBorder');
				$("#ad_text_compose").focus();
			}else{
				//var finalPrice = parseInt($('#totalAdCostId').text());
				var finalPrice = $('#totalAdCostId').text();
				console.log('finalPrice : '+finalPrice);
				
				$('#totalHPrice').val(finalPrice);
				$('#ad-design-from').submit();
			}
		}
		
		
		function escapeHtml(text) {
			  return text
			      .replace(/&/g, "&amp;")
			      .replace(/</g, "&lt;")
			      .replace(/>/g, "&gt;")
			      .replace(/"/g, "&quot;")
			      .replace(/'/g, "&#039;");
			}
		
		removeAdEdition = function(userAdEditionId){
			var ad_text_compose = $("#ad_text_compose").val();
			var tick_mark= $('#tick_mark').is(':checked')?1:0;
			var bgColor= $("input[name='bgColor']:checked").val();
			
			$.ajax({  
	  		       url : '/adcateogory/removeedition',  
	  		       type : 'POST',
	  		       data: { userAdEditionId: userAdEditionId, ad_text_compose : ad_text_compose, tick_mark :tick_mark, bgColor : bgColor},
	  		    	 success: function(response) {   
	  		    		window.location.href = "/adcateogory/matrimonial/RxxOBLR7mPM=/";
					}
	  			});
			
		}
		
		
		removeAdPublication = function(){
			
			$.ajax({  
	  		       url : '/adcateogory/removepublication',  
	  		       type : 'POST',
	  		    	 success: function(response) {   
	  		    		window.location.href = "/adcateogory/matrimonial/RxxOBLR7mPM=/";
					}
	  			});
			
		}
		
		</script>
	</body>

	<!-- end::Body -->
</html>