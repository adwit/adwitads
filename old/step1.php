<?php
include 'database.php';

include 'function.php';
	

   if(isset($tab)) $_SESSION["tab"]=$tab;
   if(isset($classified_select_name)){
		$_SESSION["classified_select_name"]=$classified_select_name;
   }



   include 'header.php';           
?>



<style>
.btn-blue {
    background-color: #0266b5;
    color: white;
    padding: 8px 12px 8px 12px;
}
</style>


			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body" style="padding-left: 0px">

				
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					
					<div class="m-content">

						
						<!--Begin::Section-->
						<div class="m-portlet">
							<div class="m-portlet__body">
								<div class="row m-row--no-padding m--padding-bottom-30">
									<div class="col-md-12 ">
										<h4 class="m-portlet__head-text m-stack__item--center" style="text-align:center;">
										
										<?php echo $_SESSION["tab"];

										?>
											
										 - 
										 <?php
											echo $_SESSION["classified_select_name"];

											?>
										</h4>
									<hr>
									</div>
								
										<div class="col-xl-12 col-xs-12 col-md-12 col-sm-12 bluebox">
										 
	                                     <div class="step1" style="    background-color: #000000;">
										<h6  style="text-align:center;    font-size: 15px;">
														Step 1 - Choose Publication & Edition details  <i class="fa fa-info-circle" aria-hidden="true"></i>
										</h6>
										
									</div>
							

									<form action="step2.php" method="post">

									<div class="row m--padding-top-35 m--padding-bottom-30 col-md-offset-2">
									
										   <div class="col-xl-3 col-xs-12 col-md-6 col-sm-6 m--padding-bottom-10">
										      <select class="form-control country" name="publication" required>
														<option value=""hidden>Select Publication</option>

														<?php

														$sql_pub = mysqli_query($conn,"select * from publication  ORDER BY name ASC");
														while($row=mysqli_fetch_array($sql_pub))
														{
														echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
														
														}
															

														?>
											</select>
											
										   </div>
										  
										   <div class="col-xl-3 col-xs-12 col-md-6 col-sm-6 m--padding-bottom-10">
										  
													<select name="city" class="form-control city">
														<option>Select City</option>
													</select>
											
										   </div>
										   <div class="col-xl-3 col-xs-12 col-md-6 col-sm-6 m--padding-bottom-10">
										       <input  type="text" name="date"  class="form-control date date-picker muldate" data-date-format="yyyy-mm-dd" data-date-start-date="+0d" placeholder="Select Publish Date"  required />
										   </div>
											<div class="col-xl-3 col-xs-12 col-md-6 col-sm-6 m--padding-bottom-10">
	                                        <?PHP 
											//if($table == 'classified_text'){
										   //  echo "<a href='step2.php'><button type = 'submit' name='submit' class='btn btn-md btn-blue  ' >Next</button></a>";
										// }else{
										//	  echo "<a href='display_ad_step2.php'><button type = 'submit' name='submit' class='btn btn-md btn-blue  ' >Display Next</button></a>";
										// }
										 ?>
										     <a href=""><button type = "submit" name="submit" class="btn btn-md btn-blue  " >Next</button></a>
										   </div>
											</div> 
											</form>
											
											
										</div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
$(".country").change(function()
{
var id=$(this).val();
var post_id = 'id='+ id;

$.ajax
({
type: "POST",
url: "ajax.php",
data: post_id,
cache: false,
success: function(cities)
{
$(".city").html(cities);
} 
});

});
});
</script>									
										
										<div class="col-xl-12 col-xs-12 col-md-12 col-sm-12 bluebox m--margin-top-20">
	                                     <div class="step1" style="    background-color: #737373;border-radius: 8px;">
										<h6  style="text-align:center;    font-size: 15px;">
														Step 2 - Design your ad  <i class="fa fa-info-circle" aria-hidden="true"></i>
										</h6>
										
									</div>
								
											
										</div>
										<div class="col-xl-12 col-xs-12 col-md-12 col-sm-12 bluebox m--margin-top-20">
	                                     <div class="step1" style="    background-color: #737373;border-radius: 8px;">
										<h6  style="text-align:center;    font-size: 15px;">
														Step 3 - Review your order  <i class="fa fa-info-circle" aria-hidden="true"></i>
										</h6>
										
									</div>
										
									
									

									</div>
									
									</div>
							</div>
						</div>
					</div>
						
					
			</div>

			</div>
		</div>
	</div>

			<!-- end:: Body -->

			<!-- begin::Footer -->
			
	
	
	
	
	
	
	
	
	
	
	
<?php include 'footer.php'; ?>